package com.ebank.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ebank.beans.ComplaintBean;
import com.ebank.utility.DButil;

public class ComplaintDaoImpl implements ComplaintDao {

	@Override
	public boolean writeComplaint(ComplaintBean complaint) {
		Connection con = null;
		PreparedStatement ps = null;
		int rowsEffected = 0;
		boolean flag = false;
		// preparing sql query for
		String sql = "insert into complaints(userid,description,dateofcomplaint) values(?,?,?)";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, complaint.getUserId());
			ps.setString(2, complaint.getDescription());
			ps.setDate(3, new Date(complaint.getDateOfComplaint().getTime()));
			// executing sql query
			rowsEffected = ps.executeUpdate();
			flag = rowsEffected > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	public List<ComplaintBean> readAllComplaints() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ComplaintBean> complaintList = new ArrayList<>();
		ComplaintBean complaint = null;
		// preparing sql query for
		String sql = "Select complaintid,userid,description,dateofcomplaint from complaints";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				complaint = new ComplaintBean();
				complaint.setComplaintId(rs.getInt(1));
				complaint.setUserId(rs.getInt(2));
				complaint.setDescription(rs.getString(3));
				complaint.setDateOfComplaint(rs.getDate(4));
				complaintList.add(complaint);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return complaintList;
	}

	@Override
	public List<ComplaintBean> readComplaintsById(int userid) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ComplaintBean> complaintList = new ArrayList<>();
		ComplaintBean complaint = null;
		// preparing sql query for
		String sql = "Select complaintid,userid,description,dateofcomplaint from complaints where userid=?";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			while (rs.next()) {
				complaint = new ComplaintBean();
				complaint.setComplaintId(rs.getInt(1));
				complaint.setUserId(rs.getInt(2));
				complaint.setDescription(rs.getString(3));
				complaint.setDateOfComplaint(rs.getDate(4));
				complaintList.add(complaint);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return complaintList;
	}
}
