package com.ebank.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ebank.beans.AccountBean;
import com.ebank.utility.DButil;

public class BlockingDaoImpl implements BlockingDao {

	@Override
	public String readUserStatus(int accountNumber) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String status = null;
		// this query is used to update gender in user table for a particular user.
		String sql = "select status from accountdetails where accountNum = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, accountNumber);
			rs = ps.executeQuery();
			if (rs.next())
				status = rs.getString(1);
		} catch (SQLException e) {
			System.out.println();
		} finally {
			try {
				// closing the connection
				con.close();
			} catch (SQLException e) {
				// e.printStackTrace();
			}
		}

		return status;
	} // End of readUserStatus

	/**
	 * This method updates account status in data base table
	 */
	@Override
	public boolean updateUserStatus(int accountNumber, String status) {
		Connection con = null;
		PreparedStatement ps = null;
		int rowsEffected = 0;
		// this query is used to update gender in user table for a particular user.
		String sql = "update accountdetails set status = ? where accountNum = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, status);
			ps.setInt(2, accountNumber);
			rowsEffected = ps.executeUpdate();
		} catch (SQLException e) {
			System.out.println();
		} finally {
			try {
				// closing the connection
				con.close();
			} catch (SQLException e) {
				// e.printStackTrace();
			}
		}

		return rowsEffected > 0 ? true : false;
	} // End of updateUserStatus

	/**
	 * This method fetches all blocked list from tables
	 */
	@Override
	public List<AccountBean> fetchBlockedList() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<AccountBean> blockedList = new ArrayList<>();
		AccountBean account = null;
		// this query is used to fetch all records which have blocked as status field.
		String sql = "select accountNum,userId,accountName,status from accountdetails where status = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, "blocked");
			rs = ps.executeQuery();
			while (rs.next()) {
				account = new AccountBean();
				account.setAccountNumber(rs.getInt(1));
				account.setUserId(rs.getInt(2));
				account.setAccountHolderName(rs.getString(3));
				account.setStatus(rs.getString(4));
				blockedList.add(account);
			}

		} catch (SQLException e) {
			System.out.println();
		} finally {
			try {
				// closing the connection
				con.close();
			} catch (SQLException e) {
				// e.printStackTrace();
			}
		}

		return blockedList;
	} // End of fetchBlockedList

}
