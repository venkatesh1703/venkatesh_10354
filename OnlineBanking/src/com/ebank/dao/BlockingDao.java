package com.ebank.dao;

import java.util.List;

import com.ebank.beans.AccountBean;

public interface BlockingDao {
	/**
	 * This method fetches status of account.
	 * 
	 * @param AccountNumber
	 * @return status in String format
	 */
	public String readUserStatus(int AccountNumber);

	/**
	 * This method used to updates the status of account
	 * 
	 * @param accountNumber
	 * @param status
	 * @return true if update success else false
	 */
	public boolean updateUserStatus(int accountNumber, String status);
	/**
	 * This method fetches all blocked user account details from tables
	 * @return List<AccountBean> object
	 */
	public List<AccountBean> fetchBlockedList();
}
