package com.ebank.dao;

import java.util.List;

import com.ebank.beans.UserTransactionBean;

/**
 * This interface contains method declarations which deals with data base
 * tables.
 * 
 * @author Team-E
 *
 */
public interface TransactionDao {

	/**
	 * This method fetches record from table for a specific row based on account
	 * Number.
	 * 
	 * @param accno
	 */
	double fetchBalance(int accno);

	/**
	 * This method updates balance in data base table based on account number.
	 * 
	 * @param accno
	 * @param balance
	 * @return true if success else false
	 */
	boolean updateBalance(int accno, double balance);
	
	/**
	 * This method checks whether specific account number exist or not
	 * @param accno
	 * @return true if exist else false
	 */
	boolean isAccountExist(int accno);
	
	/**
	 * This method insert a transaction record into table
	 * @param txBean
	 * @return
	 */
	boolean insertTransaction(UserTransactionBean txBean);
	
	/**
	 * This method deals with fetching transactions from data base tables
	 * 
	 * @param accNum
	 * @return
	 */
	List<UserTransactionBean> fetchTransactions(int accNum);
}
