package com.ebank.dao;

import java.util.List;

import com.ebank.beans.ComplaintBean;

public interface ComplaintDao {
	boolean writeComplaint(ComplaintBean complaint);

	List<ComplaintBean> readAllComplaints();

	List<ComplaintBean> readComplaintsById(int userid);
}
