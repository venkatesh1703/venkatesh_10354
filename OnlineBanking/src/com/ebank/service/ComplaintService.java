package com.ebank.service;

import java.util.List;

import com.ebank.beans.ComplaintBean;

public interface ComplaintService {

	boolean ariseComplaint(ComplaintBean complaint);

	List<ComplaintBean> getAllComplaints();
	
	List<ComplaintBean> getComplaintsById(int userid);

}
