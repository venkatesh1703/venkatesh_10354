package com.ebank.service;

import java.util.Calendar;
import java.util.List;

import com.ebank.beans.ComplaintBean;
import com.ebank.dao.ComplaintDaoImpl;

public class ComplaintServiceImpl implements ComplaintService {

	@Override
	public boolean ariseComplaint(ComplaintBean complaint) {
		complaint.setDateOfComplaint(Calendar.getInstance().getTime());
		return new ComplaintDaoImpl().writeComplaint(complaint);
	}

	@Override
	public List<ComplaintBean> getAllComplaints() {
		return new ComplaintDaoImpl().readAllComplaints();
	}

	@Override
	public List<ComplaintBean> getComplaintsById(int userid) {
		return new ComplaintDaoImpl().readComplaintsById(userid);
	}

}
