package com.ebank.service;

import java.util.List;

import com.ebank.beans.UserTransactionBean;

/**
 * This interface contains method declarations which deals with user
 * transactions
 * 
 * @author Team-E
 *
 */
public interface TransactionService {
	/**
	 * This method used to retrieve balance information of specific account number.
	 * 
	 * @param accNum
	 * @return current balance
	 */
	double doBalEnq(int accNum);

	/**
	 * This method deals with deposit operation.
	 * 
	 * @param amount
	 * @param accNum
	 * @return true if success else false
	 */
	boolean doDeposit(double amount, int accNum, UserTransactionBean txBean);

	/**
	 * This method deals with withdraw operation
	 * 
	 * @param amount
	 * @param accNum
	 * @return true if success else false
	 */
	boolean doWithdraw(double amount, int accNum, UserTransactionBean txBean);

	/**
	 * This method deals with transfer between two accounts
	 * 
	 * @param fromAccountNum
	 * @param toAccountNum
	 * @param amount
	 * @return true if success else false
	 */
	boolean doTransferAmount(int fromAccountNum, int toAccountNum, double amount, UserTransactionBean txBean);

	boolean doCurrentBillPayment(double amount, int accountNumber);

	boolean doPhoneBillPayment(double amount, int accountNumber);

	/**
	 * This method performs mini statement operation
	 * 
	 * @param AccountNumber
	 * @return List<UserTransactionBean> object
	 */
	public List<UserTransactionBean> miniStatement(int AccountNumber);

	/**
	 * This method retrieves list of transactions based on accountNumber
	 * 
	 * @param accountNumber
	 * @return List<UserTransactionBean> Object
	 */
	public List<UserTransactionBean> transactionHistory(int accountNumber);
}
