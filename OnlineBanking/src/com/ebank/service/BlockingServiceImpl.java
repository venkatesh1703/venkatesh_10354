package com.ebank.service;

import java.util.List;

import com.ebank.beans.AccountBean;
import com.ebank.dao.BlockingDaoImpl;

public class BlockingServiceImpl implements BlockingService {

	/**
	 * This method checks account is blocked or not and returns boolean value
	 * according to that.
	 */
	@Override
	public boolean isBlocked(int accountNumber) {

		return new BlockingDaoImpl().readUserStatus(accountNumber).equals("blocked");
	} // End of isBlocked

	/**
	 * This method provides implementation for BlockingService interface method.
	 * This method used to block the account.
	 */
	@Override
	public boolean blockUser(int accountNumber) {

		return new BlockingDaoImpl().updateUserStatus(accountNumber, "blocked");
	} // End of blockUser

	/**
	 * This method unBlocks the account and returns boolean value according to that
	 */
	@Override
	public boolean unBlockUser(int accountNumber) {
		return new BlockingDaoImpl().updateUserStatus(accountNumber, "active");
	} // End of unBlockUser

	/**
	 * This method calling BlockingDao method to get use blocked list
	 */
	@Override
	public List<AccountBean> getAllBlockedUserList() {
		return new BlockingDaoImpl().fetchBlockedList();

	} // End of getAllBlockedUserList

}
