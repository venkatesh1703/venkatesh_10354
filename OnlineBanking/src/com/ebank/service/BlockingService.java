package com.ebank.service;

import java.util.List;

import com.ebank.beans.AccountBean;

public interface BlockingService {
	/**
	 * This method checks whether account is blocked or active
	 * 
	 * @param accountNumber
	 * @return true if blocked else false
	 */
	boolean isBlocked(int accountNumber);

	/**
	 * This method calls the dao updateUserStatus(int,String) method and passes
	 * status as blocked
	 * 
	 * @param accountNumber
	 * @return
	 */
	boolean blockUser(int accountNumber);

	/**
	 * This method unblocks the account
	 * 
	 * @param accountNumber
	 * @return true if success else false
	 */
	boolean unBlockUser(int accountNumber);

	/**
	 * This method meant for getting all blocked user list
	 * @return List<AccountBean>
	 */
	List<AccountBean> getAllBlockedUserList();
}
