package com.ebank.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ebank.beans.UserTransactionBean;
import com.ebank.service.TransactionService;
import com.ebank.service.TransactionServiceImpl;

public class TransactionServiceTestImpl implements TransactionServiceTest {

	TransactionService txService = null;

	/**
	 * This method used to initialize values before test case execution.
	 */
	@Before
	public void setUp() {
		txService = new TransactionServiceImpl();
	}

	/**
	 * This method used to close the resource that open in setup after test case
	 * execution.
	 */
	@After
	public void tearDown() {
		txService = null;
	}

	/**
	 * This test case method used to test TransactionServiceImpl doBalEnq method for
	 * positive case.
	 */
	@Test
	@Override
	public void doBalEnqTestPositive() {
		double result = txService.doBalEnq(12345);
		assertTrue(result >= 0);
	}

	/**
	 * This test case method used to test TransactionServiceImpl doBalEnq method for
	 * negative case.
	 */
	@Test
	@Override
	public void doBalEnqTestNegative() {
		double result = txService.doBalEnq(-1);
		assertEquals(0.0d, result, 0);
	}

	/**
	 * This test case method used to test TransactionServiceImpl doDeposit method
	 * for Positive case.
	 */
	@Test
	@Override
	public void doDepositTestPositive() {
		boolean result = txService.doDeposit(10000, 12345, new UserTransactionBean());
		assertTrue(result);
	}

	/**
	 * This test case method used to test TransactionServiceImpl doDeposit method
	 * for negative case.
	 */
	@Test
	@Override
	public void doDepositTestNegative() {
		boolean result = txService.doDeposit(10000, -1, new UserTransactionBean());
		assertFalse(result);
	}

	/**
	 * This test case method used to test TransactionServiceImpl doWithdraw method
	 * for Positive case.
	 */
	@Test
	@Override
	public void doWithdrawTestPositive() {
		boolean result = txService.doWithdraw(10000, 12345, new UserTransactionBean());
		assertTrue(result);
	}

	/**
	 * This test case method used to test TransactionServiceImpl doWithdraw method
	 * for Negative case.
	 */
	@Test
	@Override
	public void doWithdrawTestNegative() {
		boolean result = txService.doWithdraw(10000, -1, new UserTransactionBean());
		assertFalse(result);
	}

	/**
	 * This test case method used to test TransactionServiceImpl doTransferAmount
	 * method for Positive case.
	 */
	@Test
	@Override
	public void doTransferAmountTestPositive() {
		boolean result = txService.doTransferAmount(12345, 12348, 1000, new UserTransactionBean());
		assertTrue(result);
	}

	/**
	 * This test case method used to test TransactionServiceImpl doTransferAmount
	 * method for negative case.
	 */
	@Test
	@Override
	public void doTransferAmountTestNegative() {
		boolean result = txService.doTransferAmount(-1, 12348, 1000, new UserTransactionBean());
		assertFalse(result);
	}

	/**
	 * This test case method used to test TransactionServiceImpl doBillPayment
	 * method for Positive case.
	 */
//	@Test
//	@Override
//	public void doBillPaymentTestPositive() {
//		double result = txService.doBillPayment(1000, 12345);
//		
//	}

//	@Override
//	public void doBillPaymentTestNegative() {
//		// TODO Auto-generated method stub
//
//	}

	@Override
	public void miniStatementTestPositive() {
		// TODO Auto-generated method stub

	}

	@Override
	public void miniStatementTestNegative() {
		// TODO Auto-generated method stub

	}

	@Override
	public void transactionHistoryTestPositive() {
		// TODO Auto-generated method stub

	}

	@Override
	public void transactionHistoryTestNegative() {
		// TODO Auto-generated method stub

	}

}
