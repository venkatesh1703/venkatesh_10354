package com.ebank.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	AdminServiceTestImpl.class,
	UserServiceTestImpl.class
})
public class AllTestSuite {

}
