package com.ebank.test;

public interface AdminServiceTest {
	/**
	 * This is implementation of AdminService doRegistration(AdminBean) method. This
	 * method perform registration of admin. In Positive senario.
	 */
	void doRegistrationTestPositive();
	/**
	 * This is implementation of AdminService doRegistration(AdminBean) method. This
	 * method perform registration of admin. In Negative senario.
	 */
	void doRegistrationTestNegative();
	/**
	 * This is the implementation of AdminService doLogin(int, String) method. This
	 * method perform login operation for an admin if authentication is success.
	 */
	void doLoginTestPositive();
	/**
	 * This is the implementation of AdminService doLogin(int, String) method. This
	 * method perform login operation for an admin it checks in negative senario.
	 */
	void doLoginTestNegative();
	/**
	 * This is the implementation of AdminService getAdminById(int) method.Positive Test
	 */
	void getAdminByIdTestPositive();
	/**
	 * This is the implementation of AdminService getAdminById(int) method.Negative test
	 */
	void getAdminByIdTestNegative();
	
	/**
	  * This method is used to return true if the password is updated successfully
	  */
	void updatePasswordTestPositive();
	/**
	  * This method is used to return true if the password is not updated 
	  */
	void updatePasswordTestNegative();
	 /**
	  * This method is used to return true if the name is updated successfully
	  */
	void updateNameTestPositive();
	/**
	  * This method is used to return true if the name is not updated 
	  */
	void updateNameTestNegative();
	/**
	 * returns true if record is updated successfully
	 */
	void updateDateTestPositive();
	/**
	 * returns true if record is not updated successfully
	 */
	void updateDateTestNegative();
}
