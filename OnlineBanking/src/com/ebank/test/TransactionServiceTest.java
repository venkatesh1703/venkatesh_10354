package com.ebank.test;

/**
 * This interface contains all test case method declarations of
 * TransactionServices class.
 * 
 * @author IMVIZAG
 *
 */
public interface TransactionServiceTest {
	void setUp();

	void tearDown();

	void doBalEnqTestPositive();

	void doBalEnqTestNegative();

	void doDepositTestPositive();

	void doDepositTestNegative();

	void doWithdrawTestPositive();

	void doWithdrawTestNegative();

	void doTransferAmountTestPositive();

	void doTransferAmountTestNegative();

//	void doBillPaymentTestPositive();

	//void doBillPaymentTestNegative();

	void miniStatementTestPositive();

	void miniStatementTestNegative();

	void transactionHistoryTestPositive();

	void transactionHistoryTestNegative();

}
