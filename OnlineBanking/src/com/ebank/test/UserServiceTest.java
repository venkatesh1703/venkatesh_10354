package com.ebank.test;

/**
 * This interface contains all the test cases for userService class
 * functionalities.
 * 
 * @author IMVIZAG
 *
 */
public interface UserServiceTest {
	void setUp();

	void tearDown();

	void isUserTestPositive();

	void isUserTestNegative();

	void doLoginTestPositive();

	void doLoginTestNegative();

	void doRegistrationTestPositive();

	void doRegistrationTestNegative();

	void fetchAllUsersTestPositive();

	void fetchAllUsersTestNegative();

	void getUserByIdTestPositive();

	void getUserByIdTestNegative();

	void updatePasswordTestPositive();

	void updatePasswordTestNegative();

	void updateFirstNameTestPositive();

	void updateFirstNameTestNegative();

	void updateLastNameTestPositive();

	void updateLastNameTestNegative();

	void updatePhoneNumberTestPositive();

	void updatePhoneNumberTestNegative();

	void updateMailIdTestPositive();

	void updateMailIdTestNegative();

	void updateDateOfBirthTestPositive();

	void updateDateOfBirthTestNegative();

	void updateGenderTestPositive();

	void updateGenderTestNegative();

	void getAccountdetailsTestPositive();

	void getAccountdetailsTestNegative();
}
