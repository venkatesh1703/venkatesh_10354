package com.ebank.utility;

import java.sql.Connection;
import java.sql.DriverManager;

public class DButil {
	public static Connection getcon() {
		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");  
			con=DriverManager.getConnection(  
			"jdbc:mysql://localhost:3306/onlineBanking","root","innominds");  
		}catch(Exception e) {
			e.printStackTrace();
		}
		return con;
	}
} 
