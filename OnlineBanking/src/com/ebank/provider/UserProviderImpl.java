package com.ebank.provider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebank.beans.AccountBean;
import com.ebank.beans.UserBean;
import com.ebank.service.UserServiceImpl;

@Path("/user")
public class UserProviderImpl implements UserProvider {
	/**
	 * This method is will execute when request for user login is came. it calls
	 * service method to authenticate. And returns response.
	 * 
	 * @param userid
	 * @param password
	 * @return json string
	 */
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean userLogin(@QueryParam("userid") int userid, @QueryParam("password") String password) {
		boolean isAuthenticated = new UserServiceImpl().doLogin(userid, password);

		return isAuthenticated;
	} // End of userLogin

	/**
	 * This method used to handle the request of user for registration. This method
	 * calls service method to register user.
	 * 
	 * @param json
	 * @return
	 */
	@POST
	@Path("/registration")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean userRegistration(UserBean user) {
		boolean isRegistered = false;

		isRegistered = new UserServiceImpl().doRegistration(user, user.getAccount());
		return isRegistered;
	} // End of userRegistration

	/**
	 * This method used to handle the request to get all user list. This method
	 * calls service method to fetchAllUsers.
	 * 
	 */
	@GET
	@Path("/fetchallusers")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public List<UserBean> fetchAllUsers() {

		return new UserServiceImpl().fetchAllUsers();
	}

	/**
	 * This method used to handle the request to get specific user by id. This
	 * method calls service method to fetch user details.
	 * 
	 */
	@GET
	@Path("/getuserbyid")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public UserBean getUserById(@QueryParam("userid") int userId) {

		return new UserServiceImpl().getUserById(userId);
	}

	/**
	 * This method used to handle the request to update password of specific user.
	 * This method calls service method to update.
	 * 
	 */
	@POST
	@Path("/updatepassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean updatePassword(@QueryParam("userid") int userid, @QueryParam("password") String password) {
		return new UserServiceImpl().updatePassword(userid, password);
	}

	/**
	 * This method used to handle the request to update firstName of specific user.
	 * This method calls service method to update.
	 * 
	 */
	@POST
	@Path("/updatefirstname")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean updateFirstName(@QueryParam("userid") int userid, @QueryParam("firstname") String firstname) {
		return new UserServiceImpl().updateFirstName(userid, firstname);
	}

	/**
	 * This method used to handle the request to update lastName of specific user.
	 * This method calls service method to update.
	 * 
	 */
	@POST
	@Path("/updatelastname")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean updateLastName(@QueryParam("userid") int userid, @QueryParam("lastname") String lastname) {
		return new UserServiceImpl().updateLastName(userid, lastname);
	}

	/**
	 * This method used to handle the request to update phone number of specific
	 * user. This method calls service method to update.
	 * 
	 */
	@POST
	@Path("/updatephonenumber")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean updatePhoneNumber(@QueryParam("userid") int userid, @QueryParam("phonenumber") String phonenum) {
		return new UserServiceImpl().updatePhoneNumber(userid, phonenum);
	}

	/**
	 * This method used to handle the request to update mail id of specific user.
	 * This method calls service method to update.
	 * 
	 */
	@POST
	@Path("/updatemailid")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean updateMailId(@QueryParam("userid") int userid, @QueryParam("mailid") String mailid) {
		return new UserServiceImpl().updateMailId(userid, mailid);
	}

	/**
	 * This method used to handle the request to update date of birth of specific
	 * user. This method calls service method to update.
	 * 
	 */
	@POST
	@Path("/updatedateofbirth")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean updateDateOfBirth(@QueryParam("userid") int userid, @QueryParam("dateofbirth") String dateofbirth) {
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-mm-dd").parse(dateofbirth);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new UserServiceImpl().updateDateOfBirth(userid, date);
	}

	/**
	 * This method used to handle the request to update gender of specific user.
	 * This method calls service method to update.
	 * 
	 */
	@POST
	@Path("/updategender")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean updateGender(@QueryParam("userid") int userid, @QueryParam("gender") String gender) {
		return new UserServiceImpl().updateGender(userid, gender);
	}

	/**
	 * This method used to handle the request to get account details of specific
	 * user. This method calls service method.
	 * 
	 */
	@GET
	@Path("/getaccountdetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public AccountBean getAccountdetails(@QueryParam("userid") int userid) {
		
		return new UserServiceImpl().getAccountdetails(userid);
	}
}
