package com.ebank.provider;

import java.util.Date;
import java.util.List;

import javax.ws.rs.QueryParam;

import com.ebank.beans.AdminBean;
import com.ebank.beans.UserBean;

public interface AdminProvider {
	/**
	 * This method is declaration of adminLogin functionality.
	 * 
	 * @param adminid
	 * @param password
	 * @return
	 */
	boolean adminLogin(@QueryParam("adminid") int adminid, @QueryParam("password") String password);

	/**
	 * This method is declaration of adminRegistration functionality.
	 * 
	 * @param json
	 * @return
	 */
	boolean adminRegistration(String json);

	/**
	 * This method is used to display all users details .
	 * 
	 * @param json
	 * @return
	 */
	List<UserBean> fetchAllUsers();

	/**
	 * This method is used to get the admin details by adminId
	 * 
	 * @param adminid
	 * @reutrn json
	 */
	AdminBean getadminbyid(int adminid);

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param name
	 * @return true if adminid exists in the database
	 */
	boolean updateName(int adminId, String name);

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param password
	 * @return true if updation is success else false
	 */
	boolean updatePassword(int adminId, String password);

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param gender
	 * @return true if adminid exists in the database
	 */
	boolean updateGender(int adminId, String gender);

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param dateOfBirth
	 * @return true if adminid exists in the database
	 */
	boolean updateDate(int adminId, Date dateOfBirth);

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param mailId
	 * @return true if adminid exists in the database
	 */
	boolean updateMailId(int adminId, String mailId);

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param phoneNum
	 * @return true if adminid exists in the database
	 */
	boolean updatePhoneNumber(int adminId, String phoneNum);
}
