package com.ebank.provider;

import java.util.List;

import com.ebank.beans.ComplaintBean;

public interface ComplaintProvider {
	boolean ariseComplaint(ComplaintBean complaint);

	List<ComplaintBean> getAllComplaints();
	
	List<ComplaintBean> getComplaintsById(int userid);
}
