package com.ebank.provider;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebank.beans.ComplaintBean;
import com.ebank.service.ComplaintServiceImpl;

@Path("/complaint")
public class ComplaintProviderImpl implements ComplaintProvider {
	@POST
	@Path("/arisecomplaint")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean ariseComplaint(ComplaintBean complaint) {
		return new ComplaintServiceImpl().ariseComplaint(complaint);
	}

	@GET
	@Path("/getallcomplaints")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public List<ComplaintBean> getAllComplaints() {
		return new ComplaintServiceImpl().getAllComplaints();
	}

	@GET
	@Path("/getcomplaintsbyid")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public List<ComplaintBean> getComplaintsById(@QueryParam("userid") int userid) {
		return new ComplaintServiceImpl().getComplaintsById(userid);
	}

}
