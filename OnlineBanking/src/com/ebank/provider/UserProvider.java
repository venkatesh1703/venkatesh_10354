package com.ebank.provider;

import java.util.List;

import com.ebank.beans.AccountBean;
import com.ebank.beans.UserBean;

public interface UserProvider {
	/**
	 * This method is declaration of userLogin operation
	 * 
	 * @param userid
	 * @param password
	 * @return
	 */
	boolean userLogin(int userid, String password);

	/**
	 * This method is declaration of userRegistration functionality
	 * 
	 * @param json
	 * @return
	 */
	boolean userRegistration(UserBean user);

	List<UserBean> fetchAllUsers();

	UserBean getUserById(int userId);

	boolean updatePassword(int userid, String password);

	boolean updateFirstName(int userid, String firstname);

	boolean updateLastName(int userid, String lastname);

	boolean updatePhoneNumber(int userid, String phonenum);

	boolean updateMailId(int userid, String mailid);

	boolean updateDateOfBirth(int userid, String dateofbirth);

	boolean updateGender(int userid, String gender);

	AccountBean getAccountdetails(int userid);
}
