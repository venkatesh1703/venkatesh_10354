package com.ebank.provider;
import java.util.List;

import javax.ws.rs.QueryParam;

import com.ebank.beans.AccountBean;
public interface BlockingProvider {
	/**
	 * This method is used to check whether the user is blocked or not.
	 * @param accountNumber
	 * @return
	 */
	 boolean isBlocked(@QueryParam("accountnumber")int accountnumber);
	 /**
	  * This method is used to Block the user.
	  * @param accountnumber
	  * @return
	  */
	 boolean blockUser(@QueryParam("accountnumber")int accountnumber);
	 /**
	  * This method is used to Unblock the user.
	  * @param accountnumber
	  */
	 boolean unBlockUser(@QueryParam("accountnumber")int accountnumber);
	 /**
	  * This method is used to get blocked list
	  */
	 List<AccountBean> getAllBlockedUserList();
	
}
