package com.abc.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@RestController
public class AccountController {
	@Autowired
	private AccountService accountService;
	
	@GetMapping("/welcome")
	public String greet() {
		return "hi";
	}

	@PostMapping("account/create")
	public ResponseEntity<?> createNewAccount(@RequestBody Account account) {
		int id = accountService.saveAccount(account);

		return new ResponseEntity<>("New account is created with id:" + id, HttpStatus.OK);
	}

	@GetMapping("/account/{id}")
	public ResponseEntity<Account> get(@PathVariable("id") int id) {
		Account account = accountService.getAccountByID(id);
		if (account == null) {

			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Account>(account, HttpStatus.OK);
	}

	@GetMapping("/account")
	public ResponseEntity<List<Account>> list() {
		List<Account> accounts = accountService.getAllAccountList();
		if (accounts.isEmpty()) {
			return new ResponseEntity<List<Account>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Account>>(accounts, HttpStatus.OK);
	}
	
	@DeleteMapping("/account/{id}")
	   public ResponseEntity<?> delete(@PathVariable("id") int accno) {
		  
		   Account account = accountService.getAccountByID(accno);
	        if (account == null) {
	           
	            return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
	        }
	        accountService.delete(accno);
	        return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);
	     
	   }
	
	@PutMapping("/account/{id}")
	   public ResponseEntity<?> update(@PathVariable("id") int accno, @RequestBody Account account) {
		   Account currentAccount = accountService.getAccountByID(accno);
	         
	        if (currentAccount==null) {
	           return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
	        }
	 
	        currentAccount.setAccno(account.getAccno());
	        currentAccount.setName(account.getName());
	        currentAccount.setBalance(account.getBalance());
	        accountService.update(currentAccount);
	        return new ResponseEntity<Account>(currentAccount, HttpStatus.OK);
		}
	
	@PostMapping("/upload/{empId}")
	public ResponseEntity<?> upload(@PathVariable("empId")String empId,@RequestBody MultipartFile file){
		String imageName = empId;
		try {
			File upl = new File("images/" + imageName+".jpg");
			upl.createNewFile();
		FileOutputStream fout = new FileOutputStream(upl);
		
		fout.write(file.getBytes());
		
	
	    
			fout.close();
			return ResponseEntity.status(HttpStatus.OK).body("uploaded success");
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.OK).body("failed");
		}
		
	}

}
