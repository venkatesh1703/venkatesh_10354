package com.abc.service;

import java.util.List;

import com.abc.hibernate.entities.Account;

public interface AccountService {

	Account getAccountByID(int accno);

	int saveAccount(Account account);

	List<Account> getAllAccountList();

	void update(Account account);

	void delete(int accno);

}
