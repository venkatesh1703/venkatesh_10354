package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServieImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;

	@Transactional
	@Override
	public Account getAccountByID(int accno) {

		return accountDAO.getAccountByAccNo(accno);
	}

	@Transactional
	@Override
	public int saveAccount(Account account) {

		return accountDAO.createAccount(account);
	}

	@Transactional
	@Override
	public List<Account> getAllAccountList() {
		return accountDAO.getAllAccounts();
	}

	@Transactional
	@Override
	public void update(Account account) {
		accountDAO.updateAccount(account);
	}

	@Transactional
	@Override
	public void delete(int accno) {
		accountDAO.deleteAccount(accno);
	}

}
