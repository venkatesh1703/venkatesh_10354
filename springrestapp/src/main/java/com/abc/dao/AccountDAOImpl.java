package com.abc.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDAOImpl implements AccountDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Account getAccountByAccNo(int accno) {

		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		return account;
	}

	@Override
	public int createAccount(Account account) {

		Session session = sessionFactory.getCurrentSession();
		session.save(account);
		return account.getAccno();
	}

	@Override
	public List<Account> getAllAccounts() {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Account> cq = cb.createQuery(Account.class);
		Root<Account> root = cq.from(Account.class);
		cq.select(root);
		Query<Account> query = session.createQuery(cq);
		return query.getResultList();
	}

	@Override
	public void updateAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		session.update(account);
		session.flush();

	}

	@Override
	public void deleteAccount(int accno) {
		Session session = sessionFactory.getCurrentSession();
		Account account = session.byId(Account.class).load(accno);
		session.delete(account);
	}

}
