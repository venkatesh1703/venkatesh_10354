package com.abc.dao;

import java.util.List;

import com.abc.hibernate.entities.Account;

public interface AccountDAO {

	Account getAccountByAccNo(int accno);

	int createAccount(Account account);

	List<Account> getAllAccounts();

	void updateAccount(Account account);

	void deleteAccount(int accno);

//	Account getAccountByID(int accno);

//	boolean createAccount(Account account);

//	boolean deleteAccount(Account account);

//	Account updateAccount(Account account);
}
