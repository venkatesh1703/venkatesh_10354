package com.springmvcapp.beans;

public class UserDetails {
	private String name;
	private int age;
	private String email;

	public String getName1() {
		return name;
	}

	public void setName1(String name1) {
		this.name = name1;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
