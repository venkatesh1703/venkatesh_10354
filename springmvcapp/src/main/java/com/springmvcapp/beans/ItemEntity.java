package com.springmvcapp.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This Entity class represents Many side in OneToMany relation. It contains
 * CartEntity type property which is annotated with respective annotation and
 * Joined with CartEntity class primary key which is foreign key column in Items
 * table.
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "items")
public class ItemEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int itemId;
	@Column(nullable = false)
	private String itemName;
	@Column(nullable = false)
	private int quantity;
	@ManyToOne
	private CartEntity cart;

	public ItemEntity() {
		super();
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public CartEntity getCart() {
		return cart;
	}

	public void setCart(CartEntity cart) {
		this.cart = cart;
	}

	public ItemEntity(int itemId, String itemName, int quantity, CartEntity cart) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.quantity = quantity;
		this.cart = cart;
	}

}
