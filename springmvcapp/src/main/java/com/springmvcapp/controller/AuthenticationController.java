package com.springmvcapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.springmvcapp.beans.UserDetails;

@Controller
public class AuthenticationController {
	@GetMapping("loginform")
	public String getLoginForm() {
		return "login";
	}

	@PostMapping("login")
	public String doAuthenticate(@RequestParam("username") String username, @RequestParam("password") String password,
			ModelMap map) {
		String name = null;
		if (password.length() > 0 && username.length() > 0) {
			name = "success";
			map.addAttribute("username", username);
		} else {
			name = "failure";
		}
		return name;
	}

	@GetMapping("/registrationform")
	public String getRegistrationForm() {
		return "registration";
	}

	@PostMapping("/register")
	public ModelAndView doRegister(@ModelAttribute UserDetails user, ModelMap map) {
		String name = null;
		if (user != null) {
		//	map.addAttribute("user",user);
			name = "userdetails";
		} else {
			name = "registration_failure";
		}
		return new ModelAndView(name);
	}

}
