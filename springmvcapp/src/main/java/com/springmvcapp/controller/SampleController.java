package com.springmvcapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SampleController {
	@GetMapping("hello")
	public String sayHello() {
		return "sample";
	}

	@GetMapping("welcome")
	public ModelAndView invite() {
		String str = "hi, welcome";
		ModelAndView mav = new ModelAndView("welcome","invitation",str);
		return mav;
	}
}
