package com.ebank.service;

import java.util.Date;

import com.ebank.beans.AdminBean;
import com.ebank.dao.AdminDAOImpl;

/**
 * This class provides implementation for functions of AdminService interface
 * methods which contains service logic to Process the request.
 * 
 * @author Team-E
 *
 */
public class AdminServiceImpl implements AdminService {
	AdminDAOImpl admindao = null;

	/**
	 * This zero param constructor is used to initialize the variable admindao.
	 */
	public AdminServiceImpl() {
		admindao = new AdminDAOImpl();
	}

	/**
	 * This method used to perform registration of admin. Method logic contains
	 * validations of admin data and calling dao method.
	 */
	public boolean doRegistration(AdminBean admin) {

		return admindao.insertAdminRecord(admin);
	} // End of doRegistration

	/**
	 * This method contains logic to validate input and calling dao method to do
	 * authentication.
	 */
	public boolean doLogin(int adminId, String password) {
		AdminBean admin = admindao.fetchAdminRecordById(adminId);
		boolean isAuthenticated = false;
		if (admin.getAdminId() == adminId && admin.getPassword().equals(password)) {
			isAuthenticated = true;
		}
		return isAuthenticated;
	} // End of doLogin

	/**
	 * This method fetches admin details of specific adminId.
	 */
	public AdminBean getAdminById(int adminId) {

		return admindao.fetchAdminRecordById(adminId);
	} // End of getAdminById

	/**
	 * This method used to update password of admin. This method contains logic to
	 * perform validation on input and updation of password on existing record.
	 */
	public boolean updatePassword(int adminId, String password) {
		AdminBean admin = admindao.fetchAdminRecordById(adminId);
		admin.setPassword(password);
		return admindao.updateAdminRecord(admin);
	}// End of updatePassword

	/**
	 * This method used to update firstName of admin. This method contains logic to
	 * perform validation on input and updation of firstName on existing record.
	 */
	public boolean updateFirstName(int adminId, String firstName) {
		AdminBean admin = admindao.fetchAdminRecordById(adminId);
		admin.setFirstName(firstName);
		return admindao.updateAdminRecord(admin);
	}// End of updateFirstName

	/**
	 * This method used to update lastName of admin. This method contains logic to
	 * perform validation on input and updation of lastName on existing record.
	 */
	public boolean updateLastName(int adminId, String lastName) {
		AdminBean admin = admindao.fetchAdminRecordById(adminId);
		admin.setLastName(lastName);
		return admindao.updateAdminRecord(admin);
	}// End of updatelastName

	/**
	 * This method used to update dateofBirth of admin. This method contains logic
	 * to perform validation on input and updation of dateofBirth on existing
	 * record.
	 */
	public boolean updateDateOfBirth(int adminId, Date dateOfBirth) {
		AdminBean admin = admindao.fetchAdminRecordById(adminId);
		admin.setDateOfBirth(dateOfBirth);
		return admindao.updateAdminRecord(admin);
	}// End of updateDateOfBirth

	/**
	 * This method used to update gender of admin. This method contains logic to
	 * perform validation on input and updation of gender on existing record.
	 */
	public boolean updateGender(int adminId, String gender) {
		AdminBean admin = admindao.fetchAdminRecordById(adminId);
		admin.setGender(gender);
		return admindao.updateAdminRecord(admin);
	} // End of updateGender

	/**
	 * This method used to update mailId of admin. This method contains logic to
	 * perform validation on input and updation of mailId on existing record.
	 */
	public boolean updateMailId(int adminId, String mailId) {
		AdminBean admin = admindao.fetchAdminRecordById(adminId);
		admin.setMailId(mailId);
		return admindao.updateAdminRecord(admin);
	} // End of updateMailId

	/**
	 * This method used to update phone number of admin. This method contains logic
	 * to perform validation on input and updation of phone number on existing
	 * record.
	 */
	public boolean updatePhoneNumber(int adminId, String phoneNum) {
		AdminBean admin = admindao.fetchAdminRecordById(adminId);
		admin.setPhoneNum(phoneNum);
		return admindao.updateAdminRecord(admin);
	} // End of updatePhoneNumber

}
