package com.ebank.provider;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/test")
public class TestProvider {
	@GET
	@Path("message")
	public String message() {
		return "hello";
	}
}
