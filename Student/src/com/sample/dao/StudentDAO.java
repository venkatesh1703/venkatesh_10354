package com.sample.dao;

import java.util.List;

import com.sample.bean.Student;

/**
 * This interface contains method declarations which are meant to perform
 * operations on tables
 * 
 * @author IMVIZAG
 *
 */
public interface StudentDAO {
	/**
	 * This method used to insert student record into the table
	 * 
	 * @param student
	 * @return true if success else false
	 */
	boolean createStudent(Student student);

	/**
	 * This method fetches student record based on id
	 * 
	 * @param id
	 * @return Student object
	 */
	Student searchById(int id);

	/**
	 * This method fetches all student details
	 * 
	 * @return List<Student> object
	 */
	List<Student> getAllStudents();

	/**
	 * This method used to delete student record from the table based on id
	 * 
	 * @param studentId
	 * @return true if deleted successfully else false
	 */
	boolean deleteStudent(int studentId);

	/**
	 * This method used to update student record in the database table
	 * 
	 * @param student
	 * @return true if update is success else false
	 */
	boolean updateStudent(Student student);

}
