package com.sample.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.util.DButil;

/**
 * This class provides implementation for StudentDAO methods. These methods
 * contains logic to do database operations
 * 
 * @author IMVIZAG
 *
 */
public class StudentDAOImpl implements StudentDAO {

	/**
	 * This method inserts a student record into the table.
	 */
	@Override
	public boolean createStudent(Student student) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "insert into studentdetail(name,age,password,email,city,state,pincode) values( ?, ?, ?,?,?,?,?)";
		try {
			// getting connection object and executing query
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, student.getName());
			ps.setInt(2, student.getAge());
			ps.setString(3, student.getPassword());
			ps.setString(4, student.getEmail());
			ps.setString(5, student.getAddress().getCity());
			ps.setString(6, student.getAddress().getState());
			ps.setInt(7, student.getAddress().getPincode());

			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {

			}
		}

		return result;
	} // End of createStudent

	/**
	 * This method performs search operation on table based on id. This method
	 * fetches details studentId,name,age,emailId.
	 */
	@Override
	public Student searchById(int id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Student student = null;
		String sql = "select studentId,name,age,emailId,city,state,pincode from studentdetails where studentid = ?";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			// retrieving values from result set and giving to student object
			while (rs.next()) {
				student = new Student();
				Address address = new Address();
				student.setId(rs.getInt("studentId"));
				student.setName(rs.getString("name"));
				student.setAge(rs.getInt("age"));
				student.setEmail(rs.getString("emailId"));
				// setting address fields to address object
				address.setCity(rs.getString("city"));
				address.setState(rs.getString("state"));
				address.setPincode(rs.getInt("pincode"));
				// setting address object to student address variable
				student.setAddress(address);
			}
		} catch (SQLException e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return student;
	} // End of searchById

	/**
	 * This method fetches all the student records available in table. This method
	 * fetches various columns include(studentId,name,age,emailId).
	 */
	@Override
	public List<Student> getAllStudents() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student student = null;
		List<Student> studentList = new ArrayList<Student>();
		String sql = "select studentId,name,age,emailId,city,state,pincode from studentdetail";
		try {
			// getting connection object and preparing statement
			con = DButil.getcon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			// getting student objects by iterating result set object
			while (rs.next()) {
				student = new Student();
				Address address = new Address();
				student.setId(rs.getInt("studentId"));
				student.setName(rs.getString("name"));
				student.setAge(rs.getInt("age"));
				student.setEmail(rs.getString("emailId"));
				// setting address fields to address object
				address.setCity(rs.getString("city"));
				address.setState(rs.getString("state"));
				address.setPincode(rs.getInt("pincode"));
				// setting address object to student address variable
				student.setAddress(address);
				studentList.add(student);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return studentList;
	} // End of getAllStudents

	/**
	 * This method performs delete operation on table based on id.
	 */
	@Override
	public boolean deleteStudent(int id) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "delete from studentdetails where studentid = ?";
		try {
			// getting connection object and setting value to studentId
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			// executing query
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}
		} catch (SQLException e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;

	} // End of deleteStudent

	/**
	 * This method performs update operation on table records based on id.
	 */
	@Override
	public boolean updateStudent(Student student) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean isUpdated = false;
		String sql = "update  studentdetail set name = ?,age = ?, emailId = ?, city = ?, state = ?, pincode = ?  where studentId = ? ";
		try {
			// getting connection object and setting values to query
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, student.getName());
			ps.setInt(2, student.getAge());
			ps.setString(3, student.getEmail());
			ps.setString(4, student.getAddress().getCity());
			ps.setString(5, student.getAddress().getState());
			ps.setInt(6, student.getAddress().getPincode());
			// Executing query
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected > 0) {
				isUpdated = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return isUpdated;
	} // End of updateStudent

	/**
	 * This method used to update password field based on studentId field.
	 */
	@Override
	public boolean updatePassword(int StudentId, String password) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean isUpdated = false;
		String sql = "update  studentdetail set password = ?  where studentId = ? ";
		try {
			// getting connection object and setting values to query
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, password);
			ps.setInt(2, StudentId);
			// Executing query
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				isUpdated = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return isUpdated;
	} // End of updatePassword

}
