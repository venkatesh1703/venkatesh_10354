package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteStudent")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		int studentId = Integer.parseInt(request.getParameter("id"));

		StudentService service = new StudentServiceImpl();
		boolean isDeleted = service.deleteStudent(studentId);		
		if (isDeleted)
			request.setAttribute("message", "successfully deleted");
		else {
			request.setAttribute("message", "Deletion Failed");
		}
		request.getRequestDispatcher("DeleteStudent.jsp").forward(request, response);

	}
}
