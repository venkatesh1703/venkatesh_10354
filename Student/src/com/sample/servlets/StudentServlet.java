package com.sample.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.service.StudentServiceImpl;

/**
 * This servlet class process the request of insert.html which is used to add
 * new student.
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// creating student and address objects
		Student student = new Student();
		Address address = new Address();
		// setting values to student class variables
		student.setName(request.getParameter("name"));
		// if age is null number format exception may arise
		student.setAge(Integer.parseInt((request.getParameter("age") != "" ? request.getParameter("age") : "0")));
		student.setEmail(request.getParameter("email"));
		// setting values to address class variables
		address.setCity(request.getParameter("city"));
		address.setState(request.getParameter("state"));
		// if PinCode is not entered value may become null then number format exception
		// may arise
		address.setPincode(
				Integer.parseInt((request.getParameter("pincode") != "" ? request.getParameter("pincode") : "0")));
		student.setAddress(address);
		// calling service class method for further processing
		boolean isAdded = new StudentServiceImpl().addStudent(student);
		if (isAdded)
			response.sendRedirect("insert.html");
		else{
			System.out.println("failed");
		}

	} // End of doPost

}
