package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;
import com.sample.util.JsonConverter;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")
public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * this servlet method is used for displaying and to get the request from the
	 * html page and sends back the response to the user in the form of html
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("application/json;charset=UTF-8");

		PrintWriter pw = response.getWriter();
		// calling service methods
		StudentService service = new StudentServiceImpl();
		List<Student> studentList = service.fetchAllStudents();
		// converting java object to json format
		JsonConverter jsonConverter = new JsonConverter();
		String jsonlist = jsonConverter.convertToJson(studentList);
		pw.println(jsonlist);
		// closing print writer
		pw.close();
	} // End of doGet

}
