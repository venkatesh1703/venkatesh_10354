package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;
import com.sample.util.JsonConverter;

/**
 * Servlet implementation class FindByIdServlet
 */
@WebServlet("/SearchStudent")
public class FindByIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FindByIdServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");

		PrintWriter pw = response.getWriter();
		int studentid = Integer.parseInt(request.getParameter("id"));
		StudentService service = new StudentServiceImpl();
		Student st = service.searchStudent(studentid);

		JsonConverter jsonConverter = new JsonConverter();
		String jsonlist = jsonConverter.convertToJson(st);
		pw.println(jsonlist);
		// closing print writer
		pw.close();

	}
}
