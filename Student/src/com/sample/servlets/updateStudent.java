package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * This servlet class is meant for updating student details. This class accepts
 * post type request
 */
@WebServlet("/updateStudent")
public class updateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public updateStudent() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// reading id from request object
		int studentId = Integer.parseInt(request.getParameter("id"));
		StudentService service = new StudentServiceImpl();
		// calling service method to get student object
		Student student = service.searchStudent(studentId);
		// initializing print writer object to generate response
		PrintWriter out = response.getWriter();
		if (student != null) {
			// creating servlet context object
			ServletContext sc = getServletContext();
			// redirecting to update page
			sc.getRequestDispatcher("/updatedetails.html").include(request, response);
			// reading data from request object
			int studentId1 = Integer.parseInt(request.getParameter("id"));
			String name = request.getParameter("name");
			int age = Integer.parseInt(request.getParameter("age"));
			Student student1 = new Student();
			student1.setId(studentId1);
			student1.setName(name);
			student1.setAge(age);
			boolean result = service.updateStudent(student1);
			if (result) {
				out.println("updated Successfully");
			} else {
				out.println("!!oops not updated");
			}

		}

	}

}
