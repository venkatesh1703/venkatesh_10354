package com.sample.bean;

/**
 * This is a bean class which represents student objects
 * 
 * @author IMVIZAG
 *
 */
public class Student {
	// declaring student fields
	private int id;
	private String name;
	private int age;
	private String password;
	private String email;
	private Address address;

	// default constructor for Student
	public Student() {
		super();
	}

	// setters and getters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
