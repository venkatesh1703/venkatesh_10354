package com.sample.bean;

/**
 * This is a bean class which represents objects of Address
 * 
 * @author IMVIZAG
 *
 */
public class Address {
	// Address fields declaration
	private String city;
	private String state;
	private int pincode;

	// default constructor for this class
	public Address() {
		super();
	}

	// setters and getters for class fields
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

}
