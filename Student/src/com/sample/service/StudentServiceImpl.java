package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;

public class StudentServiceImpl implements StudentService {
	/**
	 * This method calls dao method to store student details in data base.
	 */
	@Override
	public boolean addStudent(Student student) {

		return new StudentDAOImpl().createStudent(student);
	} // End of addStudent

	/**
	 * This method calls dao method to find student details based on student id
	 */
	@Override
	public Student searchStudent(int id) {

		return new StudentDAOImpl().searchById(id);
	} // End of searchStudent

	/**
	 * This method retrieves all student details.
	 */
	@Override
	public List<Student> fetchAllStudents() {

		return new StudentDAOImpl().getAllStudents();
	} // End of fetchAllStudents

	/**
	 * This method used to deleted a student details based on id
	 */
	@Override
	public boolean deleteStudent(int id) {

		return new StudentDAOImpl().deleteStudent(id);
	} // End of deleteStudent

	@Override
	public boolean updateStudent(Student student) {
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.updateStudent(student);
		return result;
	}

}
