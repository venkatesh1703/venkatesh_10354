package com.sample.service;

import java.util.List;

import com.sample.bean.Student;

/**
 * This interface contains method declarations which are meant to interact with
 * dao methods. These methods contains service logic mainly.
 * 
 * @author IMVIZAG
 *
 */
public interface StudentService {
	/**
	 * This method meant for adding a student.
	 * 
	 * @param student
	 * @return true if success else false
	 */
	boolean addStudent(Student student);

	/**
	 * This method meant for retrieving all student details.
	 * 
	 * @return List<Student> object
	 */
	List<Student> fetchAllStudents();

	/**
	 * This method meant to delete a student
	 * 
	 * @param studentId
	 * @return true if deletion is success else false
	 */
	boolean deleteStudent(int studentId);

	/**
	 * This method used to search student details based on specific id
	 * 
	 * @param studentId
	 * @return student object
	 */
	Student searchStudent(int studentId);

	/**
	 * This method is meant for updating student details
	 * 
	 * @param student
	 * @return
	 */
	boolean updateStudent(Student student);
}
