<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Adding Student details</title>
<style>
.messagebox {
  border: 3px solid blue ;
}


table {
	text-align: center;
	position: absolute;
	top: 40%;
	left: 40%;
	transform: translate(-50%, -50%);
	color: black;
}

tr {
	width: 500px;
	height: 50px;
}

td {
	text width: 180px;
	text-align: right;
	font-size: 20px;
	text-transform: uppercase;
}

input {
	width: 200px;
	height: 25px;
	border-radius: 8px;
}

#submit {
	width: 100px;
	height: 25px;
	border-radius: 10px;
	margin-right: -67px;
	font-size: 15px;
}

#back {
	width: 100px;
	height: 25px;
	border-radius: 10px;
	margin-right: 1px;
	font-size: 15px;
}
</style>
</head>
<body>
	<div class ="messagebox" align="center">
		<c:out value="${requestScope.message }"></c:out>
	</div>
	<form action="StudentServlet" method="post">

		<table>
			<!-- 
			<tr>
				<td>Id:</td>
				<td><input type="text" name="id" /></td>
			</tr>
-->
			<tr>
				<td>Name:</td>
				<td><input type="text" name="name" /></td>
			</tr>
			<tr>
				<td>Age:</td>
				<td><input type="text" name="age" /></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><input type="text" name="email" /></td>
			</tr>
			<tr>
				<td>City:</td>
				<td><input type="text" name="city" /></td>
			</tr>
			<tr>
				<td>State:</td>
				<td><input type="text" name="state" /></td>
			</tr>
			<tr>
				<td>PinCode:</td>
				<td><input type="text" name="pincode" /></td>
			</tr>
			<tr>
				<td><input type="submit" id="submit" value="save" /><br>
				</td>
				<td><a href="index.html"><input type="button" id="back"
						value="Back" /></a></td>
			</tr>
		</table>

	</form>
</body>
</html>