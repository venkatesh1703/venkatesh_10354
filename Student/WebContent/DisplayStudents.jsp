<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="com.sample.bean.Student"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student List</title>
</head>
<body>
	<table border='1'>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Age</th>
			<th>EmailId</th>
			<th>City</th>
			<th>State</th>
			<th>PinCode</th>
		</tr>

		<c:forEach var="student" items="${requestScope.studentList}">
			<tr>
				<td>${student.id }</td>
				<td>${student.name }</td>
				<td>${student.age }</td>
				<td>${student.email }</td>
				<td>${student.address.city }</td>
				<td>${student.address.state }</td>
				<td>${student.address.pincode }</td>
			</tr>
		</c:forEach>
	</table>
	<br>
	<a href="index.html">Go to Home</a>
</body>
</html>