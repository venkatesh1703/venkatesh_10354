package com.abc.service;

import com.abc.dao.ProductDAO;
import com.abc.entity.Product;

/**
 * This class has methods which contains service logic and calls dao methods.
 * 
 * @author IMVIZAG
 *
 */
public class ProductService {
	ProductDAO productDAO;

	// zero-arg constructor
	public ProductService() {
		productDAO = new ProductDAO();
	}

	/**
	 * This method is used to store a product details. This method logic consists
	 * calling dao method
	 * 
	 * @param product
	 * @return
	 */
	public boolean createProduct(Product product) {

		return productDAO.create(product);
	}

	/**
	 * This method used to update product details of specific id.
	 * 
	 * @param product
	 * @return
	 */
	public boolean modifyProduct(Product product) {

		return productDAO.updateProduct(product);
	}

	/**
	 * This method used to remove specific product based on id.
	 * 
	 * @param product
	 * @return
	 */
	public boolean removeProduct(Product product) {

		return productDAO.deleteProduct(product);
	}

	/**
	 * This method used to get specific product details based on id.
	 * 
	 * @param productid
	 * @return
	 */
	public Product getProduct(String productid) {

		return productDAO.getProduct(productid);
	}
}
