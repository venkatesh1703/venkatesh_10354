package com.abc.main;

import com.abc.entity.Product;
import com.abc.service.ProductService;

/**
 * This class contains main method from where we are going to test data base
 * operation using hibernate.
 * 
 * @author IMVIZAG
 *
 */
public class Main {
	public static void main(String[] args) {
		// creating product objects
		Product product1 = new Product();
		product1.setProductId("p102");
		product1.setProductName("Nokia");
		product1.setProductPrice(10000);
		product1.setCategory("Mobiles");

		// creating object for service class
		ProductService productService = new ProductService();
		// performing c u r d operation

		// insertion operation
		boolean isInserted = productService.createProduct(product1);
		System.out.println("product insertion is :" + isInserted);
		System.out.println("inserted product is :" + productService.getProduct(product1.getProductId()));
		// updating price of product
		product1.setProductPrice(5000);
		boolean isUpdated = productService.modifyProduct(product1);
		System.out.println("product updation is :" + isUpdated);
		System.out.println("updated product is :" + productService.getProduct(product1.getProductId()));
		// deletion operation
		boolean isDeleted = productService.removeProduct(product1);
		System.out.println("product deletion is :" + isDeleted);
	}
}
