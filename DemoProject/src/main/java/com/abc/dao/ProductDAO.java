package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.abc.entity.Product;
import com.abc.util.HibernateUtil;

/**
 * This class contains methods which are deals with data base tables. All these
 * methods are using hibernate features.
 * 
 * @author IMVIZAG
 *
 */
public class ProductDAO {
	/**
	 * This method contains logic to insert a record into the table.
	 * 
	 * @param product
	 * @return
	 */
	public boolean create(Product product) {
		// session creation object
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		// creating transaction object
		Transaction txn = session.beginTransaction();
		// performing insert operation
		session.save(product);
		// commit transaction
		txn.commit();
		session.close();
		return true;
	}

	/**
	 * This method is used to retrieve the record based on primary key value.
	 * 
	 * @param productid
	 * @return
	 */
	public Product getProduct(String productid) {
		// creating session
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		// performing operation
		Product product = session.get(Product.class, productid);
		// closing session
		session.close();
		return product;
	}

	/**
	 * This method contains logic to update an existing row based on primary key
	 * column
	 * 
	 * @param product
	 * @return
	 */
	public boolean updateProduct(Product product) {
		boolean flag = false;
		// creating session
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		// performing update operation
		session.update(product);
		flag = true;
		tx.commit();
		// closing session
		session.close();
		return flag;
	}

	/**
	 * This method contains logic to perform delete operation on particular column
	 * based on primary key column
	 * 
	 * @param product
	 * @return
	 */
	public boolean deleteProduct(Product product) {
		boolean flag = false;
		// creating session
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		// performing deletion operation
		session.delete(product);
		flag = true;
		tx.commit();
		// closing session
		session.close();
		return flag;
	}
}
