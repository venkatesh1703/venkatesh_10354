package com.login.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * This class is an utility class which is used for data base common
 * functionality
 * 
 * @author IMVIZAG
 *
 */
public class DButil {
	/**
	 * This method establishes database connetion and returns connection object
	 * 
	 * @return
	 */
	public static Connection getcon() {
		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydatabase", "root", "innominds");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}
}
