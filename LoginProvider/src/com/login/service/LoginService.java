package com.login.service;

import com.login.dao.LoginDao;

/**
 * This class contains methods which have service logic like authentication etc.
 * 
 * @author IMVIZAG
 *
 */
public class LoginService {
	/**
	 * This method calls dao method to retrieve username and password and checks
	 * whether correct or not
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean authenticateUser(String username, String password) {
		boolean isuser = false;
		String pwd = new LoginDao().isRecordExist(username);
		if (password != null && password.equals(pwd)) {
			isuser = true;
		}
		return isuser;
	}
}
