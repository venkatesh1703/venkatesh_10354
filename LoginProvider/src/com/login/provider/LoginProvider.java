package com.login.provider;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.login.service.LoginService;

/**
 * This is the resource class which acts as web resource which is specified with
 * specific url
 * 
 * @author IMVIZAG
 *
 */
@Path("/login")
public class LoginProvider {
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Produces("text/plain")
	public String login(@QueryParam("username") String username, @QueryParam("password") String password) {
		// calling service method
		boolean isLogin = new LoginService().authenticateUser(username, password);
		String res = isLogin ? username : "not user";
		return res;
	} //End of login
}
