package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDAOImpl implements AccountDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Account getAccountByID(int accno) {
		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		return account;
	}

	@Override
	public boolean createAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		session.save(account);
		return true;
	}

	@Override
	public boolean deleteAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(account);
		return true;
	}

	@Override
	public Account updateAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		session.update(account);
		return account;
	}

}
