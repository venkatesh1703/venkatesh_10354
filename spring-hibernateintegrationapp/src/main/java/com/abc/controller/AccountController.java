package com.abc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountService;

	@RequestMapping("/accounts/{id}")
	public String searchAccount(@PathVariable("id") int accno, ModelMap map) {
		Account account = accountService.searchAccountByID(accno);
		map.addAttribute("account", account);
		return "accountDetails";
	}

	@GetMapping("createform")
	public String callCreateForm() {
		return "insert";
	}

	@PostMapping("create")
	public String insertAccount(@ModelAttribute Account account, ModelMap map) {
		boolean flag = accountService.createAccount(account);
		return flag ? "insert_success" : "insert_failure";
	}

	@GetMapping("delete")
	public String insertAccount(@RequestParam("accno") int accno, ModelMap map) {
		boolean flag = accountService.deleteAccount(accno);
		map.addAttribute("accno", accno);
		return flag ? "delete_success" : "delete_failure";
	}

	@PostMapping("accounts/update")
	public String updateAccount(@ModelAttribute Account account, ModelMap map) {
		map.addAttribute("account", accountService.updateAccount(account));
		map.addAttribute("message","success fully updated");
		return "accountDetails";
	}
	
	public ResponseEntity<?> getIp(@RequestBody Account account,HttpServletRequest request){
		request.getLocalAddr();
		return null;
	}
}
