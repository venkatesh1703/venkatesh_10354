package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountRepository;
import com.abc.entities.Account;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
	@Autowired
	private AccountRepository repository;

	@Override
	public Account save(Account account) {
		
		return null;
	}

	@Override
	public List<Account> findAll() {
		// TODO Auto-generated method stub
		return (List<Account>) repository.findAll();
	}

	@Override
	public Account findAccountById(int accno) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAccount(int accno) {
		// TODO Auto-generated method stub

	}

	@Override
	public Account updateAccount(Account account) {
		// TODO Auto-generated method stub
		return null;
	}

}
