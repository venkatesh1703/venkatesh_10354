package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.entities.Account;
import com.abc.service.AccountService;

@RestController
@RequestMapping("/api")
public class AccountController {
	@Autowired
	private AccountService service;
 
	@GetMapping("/allAccounts") 
	public ResponseEntity<List<Account>> getAll() {
		List<Account> accounts = service.findAll();
		if (accounts.isEmpty()) {
			return new ResponseEntity<List<Account>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Account>>(accounts, HttpStatus.OK);
		}
	}
}
