package com.abc.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.model.Person;
import com.abc.repository.PersonDao;

@Service
public class PersonServiceImpl {
	@Autowired
	private PersonDao<Person> personDao;

	@Transactional
	public boolean addPerson(Person person) {
		return personDao.save(person) != null;
	}

	@Transactional
	public Optional<Person> getById(long id) {
		return personDao.findById(id);
	}
}
