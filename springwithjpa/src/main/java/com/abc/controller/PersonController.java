package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.abc.model.Person;
import com.abc.service.PersonServiceImpl;

@RestController
public class PersonController {
	@Autowired
	private PersonServiceImpl personService;

	@PostMapping("add")
	public ResponseEntity<?> addPerson(@RequestBody Person person) {
		personService.addPerson(person);
		return new ResponseEntity<>("New person added ", HttpStatus.OK);
	}

}
