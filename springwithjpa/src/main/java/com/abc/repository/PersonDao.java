package com.abc.repository;

import org.springframework.data.repository.CrudRepository;

import com.abc.model.Person;

public interface PersonDao<P> extends CrudRepository<Person, Long>{

}
