package com.practice.hibernatepractice.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This entity class is representing one side in onetoMany relation. It contains
 * Many ItemEntities.
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "Cart")
public class CartEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cartId;
	@Column(nullable = false)
	private String cartName;
	@Column(nullable = false)
	private double totalPrice;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "cartIdFk", nullable = false)
	private List<ItemEntity> items;

	public CartEntity(String name, double totalPrice, List<ItemEntity> items) {
		super();
		this.cartName = name;
		this.totalPrice = totalPrice;
		this.items = items;
	}

	public CartEntity() {
		super();
	}

	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public String getName() {
		return cartName;
	}

	public void setName(String name) {
		this.cartName = name;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public List<ItemEntity> getItems() {
		return items;
	}

	public void setItems(List<ItemEntity> items) {
		this.items = items;
	}

}
