package com.practice.hibernatepractice.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.practice.hibernatepractice.entities.Employee;
import com.practice.hibernatepractice.util.HibernateUtil;

public class ManyToOneEx {

	public boolean insertRecords(Employee emp) {
		boolean flag = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		try (Session session = factory.openSession()) {
			Transaction tx = session.beginTransaction();
			session.save(emp);
			tx.commit();
			flag = true;
		}catch(Exception e) {
			e.printStackTrace();
		}

		return flag;
	}
}
