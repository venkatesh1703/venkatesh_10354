package com.practice.hibernatepractice.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.practice.hibernatepractice.entities.CartEntity;
import com.practice.hibernatepractice.util.HibernateUtil;

public class OneToManyEx {
	public boolean insertRecords(CartEntity cart) {
		boolean flag = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		try (Session session = factory.openSession()) {
			Transaction tx = session.beginTransaction();
			session.save(cart);
			tx.commit();
			flag = true;
		}

		return flag;
	}
}
