package com.practice.hibernatepractice.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.practice.hibernatepractice.entities.EmployeeEntity;
import com.practice.hibernatepractice.util.HibernateUtil;

public class HqlPracticeDao {
	/**
	 * This method is used to insert employee record into table
	 * 
	 * @param emp
	 * @return
	 */
	public boolean insertEmployee(EmployeeEntity emp) {
		boolean flag = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		try (Session session = factory.openSession()) {
			Transaction tx = session.beginTransaction();
			session.save(emp);
			tx.commit();
			flag = true;
		}

		return flag;
	}

	/**
	 * This method used to update employee details using hql query.
	 * 
	 * @param emp
	 * @return
	 */
	public boolean updateEmployee(EmployeeEntity emp) {
		boolean flag = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		try (Session session = factory.openSession()) {
			Transaction tx = session.beginTransaction();
			Query query = session
					.createQuery("update EmployeeEntity e set e.employeeName= :mail where e.employeeId= :empid");
			query.setString("mail", emp.getEmployeeName());
			query.setInteger("empid", emp.getEmployeeId());
			int rowseffected = query.executeUpdate();
			if (rowseffected > 0) {
				flag = true;
			}
			tx.commit();
		}
		return flag;
	}

	public boolean deleteEmployee(EmployeeEntity emp) {
		boolean flag = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		try (Session session = factory.openSession()) {
			Transaction tx = session.beginTransaction();
			Query query = session
					.createQuery("delete EmployeeEntity e where e.employeeId= :empid");
			query.setInteger("empid", emp.getEmployeeId());
			int rowseffected = query.executeUpdate();
			if (rowseffected > 0) {
				flag = true;
			}
			tx.commit();
		}
		return flag;
	}

	/**
	 * This method is used to fetch all records from table using hql select query.
	 * 
	 * @return
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<EmployeeEntity> getAllEmployees() {
		List<EmployeeEntity> list = null;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		try (Session session = factory.openSession()) {
			Query<EmployeeEntity> query = session.createQuery("select e.employeeId from EmployeeEntity e where e.employeeName='venkat'");
			list =query.list();
		}
		return list;
	}

}
