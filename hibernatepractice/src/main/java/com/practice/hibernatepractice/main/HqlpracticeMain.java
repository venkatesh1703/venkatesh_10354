package com.practice.hibernatepractice.main;

import java.util.List;

import com.practice.hibernatepractice.dao.HqlPracticeDao;
import com.practice.hibernatepractice.entities.EmployeeEntity;

public class HqlpracticeMain {

	public static void main(String[] args) {
		List<EmployeeEntity> list = new HqlPracticeDao().getAllEmployees();

		for (EmployeeEntity e : list) {
			System.out.println("id :" + e.getEmployeeId() + " name :" + e.getEmployeeName());
		}
//		EmployeeEntity e=new EmployeeEntity();
//		e.setEmployeeId(3);
//		System.out.println(new HqlPracticeDao().deleteEmployee(e));
	}

}
