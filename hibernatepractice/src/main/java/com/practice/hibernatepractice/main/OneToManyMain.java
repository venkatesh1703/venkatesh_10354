package com.practice.hibernatepractice.main;

import java.util.ArrayList;
import java.util.List;

import com.practice.hibernatepractice.dao.OneToManyEx;
import com.practice.hibernatepractice.entities.CartEntity;
import com.practice.hibernatepractice.entities.ItemEntity;

public class OneToManyMain {
	public static void main(String[] args) {
		CartEntity cart = new CartEntity();
		cart.setName("venkat");
		ItemEntity item1 = new ItemEntity();
		item1.setCart(cart);
		item1.setItemName("pen");
		item1.setQuantity(3);
		ItemEntity item2 = new ItemEntity();
		item2.setCart(cart);
		item2.setItemName("center fresh");
		item2.setQuantity(5);
		List<ItemEntity> list = new ArrayList<>();
		list.add(item2);
		list.add(item1);
		cart.setItems(list);
		cart.setTotalPrice(3*5+5*1);
		boolean flag = new OneToManyEx().insertRecords(cart);
		System.out.println(flag);
	}
}
