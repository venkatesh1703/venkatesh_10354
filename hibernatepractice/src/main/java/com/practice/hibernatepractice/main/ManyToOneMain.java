package com.practice.hibernatepractice.main;
import com.practice.hibernatepractice.dao.ManyToOneEx;
import com.practice.hibernatepractice.entities.Address;
import com.practice.hibernatepractice.entities.Employee;

public class ManyToOneMain {

	public static void main(String[] args) {
		Address addr = new Address();
		addr.setCity("Hyd");
		addr.setPincode("523327");

		Employee emp1 = new Employee();
		emp1.setEmpName("venkat");
		emp1.setAddress(addr);

		Employee emp2 = new Employee();
		emp2.setEmpName("javeed");
		//emp2.setAddress(addr);

		ManyToOneEx ex = new ManyToOneEx();
		//ex.insertRecords(emp1);
		ex.insertRecords(emp2);
	}

}
