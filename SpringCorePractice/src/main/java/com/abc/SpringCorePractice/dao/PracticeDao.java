package com.abc.SpringCorePractice.dao;

import org.springframework.stereotype.Repository;

import com.abc.SpringCorePractice.beans.Cart;
import com.abc.SpringCorePractice.beans.Item;

@Repository
public class PracticeDao {
	public Cart save() {

		Cart cart = new Cart();
		cart.setCartId(100);
		cart.setCartName("cloths");
		Item item = new Item();
		item.setItemId(1000);
		item.setItemName("tshirt");
		cart.setItem(item);
		return cart;
	}
}
