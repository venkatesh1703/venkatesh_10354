package com.abc.SpringCorePractice.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Cart {
	private int cartId;
	private String cartName;
	@Autowired
	@Qualifier("item2")
	private Item item;

	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public String getCartName() {
		return cartName;
	}

	public void setCartName(String cartName) {
		this.cartName = cartName;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	@Override
	public String toString() {
		return "Cart [cartId=" + cartId + ", cartName=" + cartName + ", item=" + item + "]";
	}

}
