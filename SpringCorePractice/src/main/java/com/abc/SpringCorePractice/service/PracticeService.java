package com.abc.SpringCorePractice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.SpringCorePractice.beans.Cart;
import com.abc.SpringCorePractice.dao.PracticeDao;

@Service
public class PracticeService {
	@Autowired
	private PracticeDao dao;

	public void setDao(PracticeDao dao) {
		this.dao = dao;
	}

	public Cart save() {

		return dao.save();
	}
}
