package com.abc.SpringCorePractice.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.SpringCorePractice.beans.Cart;
import com.abc.SpringCorePractice.service.PracticeService;

public class SetterInjectionMain {
	public static void main(String[] args) {
		ApplicationContext contex = new ClassPathXmlApplicationContext(
				"classpath:com/abc/SpringCorePractice/config/context.xml");
//		Cart cart = (Cart) contex.getBean("cart");
//		System.out.println(cart);

		PracticeService service = (PracticeService) contex.getBean(PracticeService.class);
		System.out.println(service.save());
	}
}
