package com.abc.service;

import com.abc.hibernate.entities.Account;

public interface AccountService {

	Account searchAccountByID(int accno);

	boolean createAccount(Account account);

	boolean deleteAccount(int accno);

	Account updateAccount(Account account);

}
