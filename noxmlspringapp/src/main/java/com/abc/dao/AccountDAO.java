package com.abc.dao;

import com.abc.hibernate.entities.Account;

public interface AccountDAO {

	Account getAccountByID(int accno);

	boolean createAccount(Account account);

	boolean deleteAccount(Account account);

	Account updateAccount(Account account);
}
